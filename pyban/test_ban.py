#!/usr/bin/env python
# -*- coding: utf8 -*-

import unittest
import ban

class TestBANFunction(unittest.TestCase):
    """ This is the test class for ban module """
    def setUp(self):
        """ setUp the BAN object """
        self.ban = ban.BAN()

    def test_search(self):
        """ Test for the search method """
        r = self.ban.search('8 bd du port')
        self.assertEquals(r['limit'], 5)
        self.assertEquals(len(r['features']), 5)
        self.assertEquals(r['features'][0]['properties']['city'], 'Amiens')

        r = self.ban.search('8 bd du port', limit=15)
        self.assertEquals(r['limit'], 15)
        self.assertEquals(len(r['features']), 15)
        self.assertEquals(r['features'][9]['properties']['city'],
                'Nort-sur-Erdre')

        r = self.ban.search('8 bd du port', autocomplete=False)
        self.assertEquals(r['limit'], 5)
        self.assertEquals(len(r['features']), 5)
        self.assertEquals(r['features'][1]['properties']['city'], 'Cergy')

        r = self.ban.search('8 bd du port', lat='48.789', lon='2.789')
        self.assertEquals(r['limit'], 5)
        self.assertEquals(len(r['features']), 5)
        self.assertEquals(r['features'][0]['properties']['city'], 'Cergy')

        r = self.ban.search('8 bd du port', filters={'postcode':'44380'})
        self.assertEquals(r['limit'], 5)
        self.assertEquals(len(r['features']), 5)
        self.assertEquals(r['features'][0]['properties']['city'], 'Pornichet')

        r = self.ban.search('paris', filters={'type':'street'})
        self.assertEquals(r['limit'], 5)
        self.assertEquals(len(r['features']), 5)
        self.assertEquals(r['features'][0]['properties']['city'], 'Amiens')

    def test_reverse(self):
        """ Test for the reverse method """
        r = self.ban.reverse(lon=2.37, lat=48.357)
        self.assertEquals(r['limit'], 1)
        self.assertEquals(len(r['features']), 1)
        self.assertEquals(r['features'][0]['properties']['city'],
                'Prunay-sur-Essonne')

        r = self.ban.reverse(lon=2.37, lat=48.357, filters={'type':'street'})
        self.assertEquals(r['limit'], 1)
        self.assertEquals(len(r['features']), 1)
        self.assertEquals(r['features'][0]['properties']['city'],
                'Prunay-sur-Essonne')

    def test_search_csv(self):
        """ Test for the search_csv method """
        self.assertRaises(NotImplementedError)

    def test_reverse_csv(self):
        """ Test for the reverse_csv method """
        self.assertRaises(NotImplementedError)

if __name__ == '__main__':
    unittest.main()
