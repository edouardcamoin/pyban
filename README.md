# Introduction
This project is a simple interface to adresse.data.gouv.fr API. The API
documentation is available to http://adresse.data.gouv.fr/api/

# Features
Actually the search and reverse function is implemented. So you can find address
from a latitude and a longitude or search if an address exist with the search
function

# Getting the module
```
#!bash
git clone https://edouardcamoin@bitbucket.org/edouardcamoin/pyban.git
```

# Quickstart
```
#!python
import ban

b = ban.BAN()
print b.search("8 bd du port")
```
