#!/usr/bin/env python
# -*- coding: utf-8 -*-

from setuptools import setup, find_packages

setup(
        name='pyban',
        version="0.0.1",
        packages=find_packages(),
        author="Edouard Camoin",
        author_email="edouard.camoin@gmail.com",
        description="Use of adresse.gouv.fr API",
        long_description=open('README.md').read(),
        install_requires = "requests",
        include_package_date=True,
        url="https://bitbucket.org/edouardcamoin/pyban",
        classifiers=[
            "Programming Language :: Python",
            ]
)
