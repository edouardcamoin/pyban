import requests
import json

""" This module is an API interface fr adresse.data.gouv.fr """


class BAN:
    """ This is the API interface to use """

    def __init__(self, endpoint="http://api-adresse.data.gouv.fr"):
        """
            Init the API interface

            :param endpoint: The URL for the adresse.data.gouv.fr API (default:
            http://qpi-adresse.data.gouv.fr")
            :type endpoint: str
            :Example:

            >>> import ban
            >>> b = ban.BAN()
            <ban.BAN instance at 0x10bf14cf8>
        """
        self.endpoint = endpoint

    def __build_filter_params(self, params, filters):
        """
            This internal method build the filter parameters for HTTP request

            :param params: The parameter given to API call
            :type params: dict
            :param filters: The filters for the API call
            :type filters: dict
            :Example:

            >>> import ban
            >>> params = {'kikoo':'lol'}
            >>> b = ban.BAN()
            >>> b.__build_filter_params(params, {'tutu':'tata'})
        """
        if not isinstance(filters, dict):
            filters = dict(filters)
        for request_filter in filters:
            params[request_filter] = filters[request_filter]

    def search(self, q, limit=None, autocomplete=True, lat=None, lon=None,
            filters=None):
        """
            This method search in the base an adress
            Filters : type (street, housenumber, ...), postcode (Zip code) and
            citycode (INSEE code). All type are described at
            http://adresse.data.gouv.fr/api/

            :param q: The adress
            :type q: str
            :param limit: Number of result to show
            :type limit: int
            :param autocomplete: The API will not autocomplete address
            :type autocomplete: bool
            :param lat: The latitude as a geographical priority in the search
            :type lat: float
            :param lat: The longitude as a geographical priority in the search
            :type lat: float
            :Example:

            >>> import ban
            >>> b = ban.BAN()
            >>> b.search('8 bd du port')
            {u'attribution': u'BAN', u'features': [{u'geometry': {u'type': ...
        """
        request = "{}/search/".format(self.endpoint)
        params = {}
        if limit is not None:
            params['limit'] = limit
        if not autocomplete:
            params['autocomplete'] = 0
        if lat is not None:
            params['lat'] = lat
        if lon is not None:
            params['lon'] = lon
        if filters is not None:
            self.__build_filter_params(params, filters)
        params['q'] = q

        return json.loads(requests.get(request, params=params).content)

    def reverse(self, lon, lat, filters=None):
        """
            This method return the reverse geocoding for a latitude and a
            longitude
            Filters : type (street, housenumber, ...)

            :param lon:
            :type lon: float
            :param lat:
            :type lat: float
            :Example:

            >>> import ban
            >>> b = ban.BAN()
            >>> b.search(2.37,48.357,filters={'type':'street'})
            {u'licence': u'ODbL 1.0', u'attribution': u'BAN', u'features':
            [{u'geometry': {u'type': u'Point', u'coordinates': [2.372558,
            48.356779]}, u'type': u'Feature', u'properties': {u'city':
            u'Prunay-sur-Essonne', u'citycode': u'91507', u'name':
            u'Rue des Ouches', u'distance': 190, u'label': u'Rue des
            Ouches 91720 Prunay-sur-Essonne', u'score':
            0.9999854876480901, u'postcode': u'91720', u'context':
            u'91, Essonne, \xcele-de-France', u'type': u'street',
            u'id': u'91507_XXXX_bc169f'}}], u'version': u'draft',
            u'limit': 1, u'filters': {u'type': u'street'}, u'type':
            u'FeatureCollection'}
        """
        request = "{}/reverse/".format(self.endpoint)
        params = {}
        if filters is not None:
            self.__build_filter_params(params, filters)
        params['lat'] = lat
        params['lon'] = lon

        return json.loads(requests.get(request, params=params).content)

    def search_csv(self, csv, columns=[], citycode=None, postcode=None):
        """
            This method search all addresses from a CSV file. This method is not
            yet implemented.
        """
        raise NotImplementedError()

    def reverse_csv(self):
        """
            This method reverse geocoding point from a CSV file. This method is not
            yet implemented.
        """
        raise NotImplementedError()

